import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/dialogs/app_alert_dialog.dart';
import 'package:alcodes_on_board_flutter/models/form_models/sign_in_form_model.dart';
import 'package:alcodes_on_board_flutter/repository/auth_repo.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fimber/flutter_fimber.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = SignInFormModel();

  String _originalLabel = 'Login';

  void _showLabel() {
    setState(() {
      _originalLabel = 'Login';
    });
  }

  Future<void> _onSubmitFormAsync() async {
    // Hide keyboard.
    AppFocusHelper.instance.requestUnfocus();

    // Validate form and save inputs to form model.
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      setState(() {
        _originalLabel = 'Loading';
      });

      // Call API.
      var alertDialogStatus = AppAlertDialogStatus.error;
      var alertDialogMessage = '';

      try {
        final repo = AuthRepo();
        final apiResponse = await repo.signInAsync(
          email: _formModel.email,
          password: _formModel.password,
        );

        // Login success, store user credentials into shared preference.
        final sharedPref = await SharedPreferences.getInstance();

        await sharedPref.setString(
            SharedPreferenceKeys.userEmail, _formModel.email);
        await sharedPref.setString(
            SharedPreferenceKeys.userToken, apiResponse.data.token);

        // Navigate to home screen.
        Navigator.of(context)
            .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);

        return;
      } catch (ex) {
        setState(() {
          _originalLabel = 'Login';
        });

        Fimber.e('d;;Error request sign in.', ex: ex);

        alertDialogMessage = '$ex';
      }

      //TODO Wrong login credential is showing as Toast, Toast will auto dismiss and user will miss out the error easily, should use pop up dialog.
      if (alertDialogMessage.isNotEmpty) {
        // Has message, show alert dialog.
        final appAlertDialog = AppAlertDialog();

        appAlertDialog.showAsync(
          context: context,
          status: alertDialogStatus,
          message: alertDialogMessage,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle = TextStyle(fontSize: 20.0);
    TextStyle linkStyle = TextStyle(color: Colors.blue);

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Center(
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://manateetech.edu/wp-content/uploads/2018/02/iconmonstr-facebook-2-240-LIGHTBLUE.jpg'),
                  radius: 60.0,
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                initialValue: 'eve.holt@reqres.in',
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSaved: (newValue) => _formModel.email = newValue,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                decoration: InputDecoration(
                  hintText: 'Email',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                //TODO Password field should hide the inputs for security purpose.
                obscureText: true,
                initialValue: 'cityslicka',
                textInputAction: TextInputAction.done,
                onSaved: (newValue) => _formModel.password = newValue,
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                decoration: InputDecoration(
                  hintText: 'Password',
                ),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              ElevatedButton(
                onPressed: () {
                  _showLabel();
                  _onSubmitFormAsync();
                },
                //TODO Should show loading when calling API, and hide loading when server responded.
                child: Text('$_originalLabel'),
                //TODO Login button is too small, make it fit the form width.
                style: ElevatedButton.styleFrom(
                    elevation: 20, minimumSize: Size(double.infinity, 36)),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              //TODO Terms of use
              RichText(
                  text: TextSpan(
                style: defaultStyle,
                children: [
                  TextSpan(
                      text: 'Terms of Use',
                      style: linkStyle,
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.of(context).pushNamed(AppRouter.termsOfUse);
                        }),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
