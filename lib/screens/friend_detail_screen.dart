import 'package:alcodes_on_board_flutter/inherited_widget/friend_inherited_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum FriendMode { Editing, Adding }

class Friend extends StatefulWidget {
  // final FriendMode friendMode;
  // final int index;

  final FriendArguments arguments;

  // Friend(friendMode, this.index);

  const Friend({Key key, @required this.arguments}) : super(key: key);

  @override
  _FriendState createState() => _FriendState();
}

class _FriendState extends State<Friend> {
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  List<dynamic> get _friends => FriendInheritedWidget.of(context).friends;

  @override
  void dispose(){
    super.dispose();

    _firstNameController.dispose();
    _lastNameController.dispose();
    _emailController.dispose();
  }

  @override
  void didChangeDependencies() {
    if (widget.arguments.friendMode == FriendMode.Editing) {
      _firstNameController.text = _friends[widget.arguments.index]['first_name'];
      _lastNameController.text = _friends[widget.arguments.index]['last_name'];
      _emailController.text = _friends[widget.arguments.index]['email'];
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.arguments.friendMode == FriendMode.Adding
            ? 'Add Friend'
            : 'Edit Friend'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            children: <Widget>[
              Image.network(_friends[widget.arguments.index]['avatar'],
                  fit: BoxFit.contain, width: 100.0),
              Container(height: 8.0),
              TextField(
                controller: _firstNameController,
                decoration: InputDecoration(hintText: 'First Name'),
              ),
              Container(height: 8.0),
              TextField(
                controller: _lastNameController,
                decoration: InputDecoration(hintText: 'Last Name'),
              ),
              Container(height: 8.0),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(hintText: 'Email'),
              ),
              Container(height: 16.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _FriendButton('Save', Colors.blue, () {
                    final firstName = _firstNameController.text;
                    final lastName = _lastNameController.text;
                    final email = _emailController.text;

                    if (widget?.arguments?.friendMode == FriendMode.Adding) {
                      _friends.add({
                        'avatar':
                            'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Facebook_f_logo_%282019%29.svg/1200px-Facebook_f_logo_%282019%29.svg.png',
                        'first_name': firstName,
                        'last_name': lastName,
                        'email': email
                      });
                    } else if (widget?.arguments?.friendMode == FriendMode.Editing) {
                      _friends[widget.arguments.index] = {
                        'avatar': _friends[widget.arguments.index]['avatar'],
                        'first_name': firstName,
                        'last_name': lastName,
                        'email': email
                      };
                    }
                    Navigator.of(context).pop();
                  }),
                  Container(height: 16.0),
                  _FriendButton('Discard', Colors.grey, () {
                    Navigator.of(context).pop();
                  }),
                  widget.arguments.friendMode == FriendMode.Editing
                      ? Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: _FriendButton('Delete', Colors.red, () {
                            _friends.removeAt(widget.arguments.index);
                            Navigator.of(context).pop();
                          }),
                        )
                      : Container()
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _FriendButton extends StatelessWidget {
  final String _text;
  final Color _color;
  final Function _onPressed;

  _FriendButton(this._text, this._color, this._onPressed);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: _onPressed,
      child: Text(
        _text,
        style: TextStyle(color: Colors.white),
      ),
      height: 40.0,
      minWidth: 100.0,
      color: _color,
    );
  }
}

class FriendArguments {
  FriendMode friendMode;
  int index;

  FriendArguments({this.friendMode, this.index});
}
