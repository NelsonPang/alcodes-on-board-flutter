import 'dart:convert';
import 'dart:ffi';

import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/inherited_widget/friend_inherited_widget.dart';
import 'package:alcodes_on_board_flutter/screens/friend_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MyFriendList extends StatefulWidget {
  @override
  _MyFriendListState createState() => _MyFriendListState();
}

class _MyFriendListState extends State<MyFriendList> {
  List<dynamic> get _friends => FriendInheritedWidget.of(context).friends;

  Future<void> getUsersAsync() async {
    // http.get("https://reqres.in/api/users").then((res) {
    //   Map userData = json.decode(res.body);
    //   setState(() {
    //     FriendInheritedWidget.of(context).friends = userData["data"];
    //   });
    // });

    final res = await http.get("https://reqres.in/api/users");

    Map userData = json.decode(res.body);
    setState(() {
          FriendInheritedWidget.of(context).friends = userData["data"];
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Friend List'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            child: Text("Load Data From API"),
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.transparent)),
            onPressed: () => getUsersAsync(),
          )
        ],
      ),
      //TODO Show list of my friends.
      body: _friends.length == 0
          ? Center(child: Text("No friend!"))
          : ListView.builder(
              itemBuilder: (context, index) {
                //TODO Click list item go to my friend detail page.
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed(
                      AppRouter.friend,
                      arguments: FriendArguments(
                        friendMode: FriendMode.Editing,
                        index: index,
                      ),
                    ).then((value) => setState(() {}));

                    // Navigator.push(
                    // context,
                    //       MaterialPageRoute(
                    //           builder: (context) => Friend(FriendMode.Editing, index)),
                    //     ).then((value) => setState(() {}));

                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 30.0, bottom: 30.0, left: 13.0, right: 22.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.network(_friends[index]['avatar'],
                              fit: BoxFit.contain, width: 100),
                          SizedBox(width: 20),
                          Expanded(
                            child:
                                _FriendFirstName(_friends[index]['first_name']),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child:
                                _FriendLastName(_friends[index]['last_name']),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: _FriendEmail(_friends[index]['email']),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: _friends.length,
            ),

      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //           builder: (context) => Friend(FriendMode.Adding, null)),
      //     ).then((value) => setState(() {}));
      //   },
      //   child: Icon(Icons.add),
      //   backgroundColor: Colors.blue,
      // ),

    );
  }
}

class _FriendFirstName extends StatelessWidget {
  final String _firstName;

  _FriendFirstName(this._firstName);

  @override
  Widget build(BuildContext context) {
    return Text(_firstName);
  }
}

class _FriendLastName extends StatelessWidget {
  final String _lastName;

  _FriendLastName(this._lastName);

  @override
  Widget build(BuildContext context) {
    return Text(_lastName);
  }
}

class _FriendEmail extends StatelessWidget {
  final String _email;

  _FriendEmail(this._email);

  @override
  Widget build(BuildContext context) {
    return Text(_email);
  }
}
