import 'package:alcodes_on_board_flutter/utils/api_components/exceptions/base/base_api_exception.dart';
import 'package:meta/meta.dart';

class ApiRequestException extends BaseApiException {
  ApiRequestException({
    @required String message,
  }) : super(message: message);
}
