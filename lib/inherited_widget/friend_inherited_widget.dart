import 'package:flutter/cupertino.dart';

class FriendInheritedWidget extends InheritedWidget {
  // final friends = [
  //   {
  //     'first_name': 'Nelson',
  //     'last_name': 'Pang Keit Hoe',
  //     'email': 'nelsonpangkeithoe@gmail.com'
  //   },
  //   {'first_name': 'Ng', 'last_name': 'Jin Hui', 'email': 'ngjinhui@gmail.com'},
  //   {
  //     'first_name': 'Yong',
  //     'last_name': 'Yu Yao',
  //     'email': 'yongyuyao@gmail.com'
  //   }
  // ];

  List friends = [];

  FriendInheritedWidget(Widget child) : super(child: child);

  static FriendInheritedWidget of(BuildContext context) {
    // ignore: deprecated_member_use
    return (context.inheritFromWidgetOfExactType(FriendInheritedWidget)
        as FriendInheritedWidget);
  }

  @override
  bool updateShouldNotify(FriendInheritedWidget oldwidget) {
    return oldwidget.friends != friends;
  }
}
