import 'package:alcodes_on_board_flutter/screens/friend_detail_screen.dart';
import 'package:alcodes_on_board_flutter/screens/home_screen.dart';
import 'package:alcodes_on_board_flutter/screens/login_screen.dart';
import 'package:alcodes_on_board_flutter/screens/my_friend_list_screen.dart';
import 'package:alcodes_on_board_flutter/screens/splash_screen.dart';
import 'package:alcodes_on_board_flutter/screens/terms_of_use_screen.dart';
import 'package:flutter/material.dart';

class AppRouter {
  static const splashScreen = '/';
  static const login = '/login';
  static const home = '/home';
  static const myFriendList = '/my_friend_list';
  static const friend = '/friend';
  static const termsOfUse = '/terms_of_use';

  static Route<dynamic> generatedRoute(RouteSettings settings) => _materialPageRoute(_getWidgetByRouteName(settings.name, settings));

  static Widget _getWidgetByRouteName(String routeName, RouteSettings settings) {
    Widget widget;

    switch (routeName) {
      case splashScreen:
        widget = SplashScreen();
        break;
      case login:
        widget = LoginScreen();
        break;
      case home:
        widget = HomeScreen();
        break;
      case myFriendList:
        widget = MyFriendList();
        break;
      case friend:
        widget = Friend(arguments: settings.arguments);
        break;
      case termsOfUse:
        widget = TermsOfUse();
        break;
      default:
        throw UnimplementedError('Unimplemented route: $routeName');
    }

    return widget;
  }

  static MaterialPageRoute _materialPageRoute(Widget widget) => MaterialPageRoute(builder: (_) => widget);
}
